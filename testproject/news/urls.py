from rest_framework import routers
from django.conf import settings
from django.conf.urls.static import static

from django.urls import path, include, re_path
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenVerifyView

from news.views import *

# router = routers.SimpleRouter()
# router.register(r'news', NewsViewSet)
# print(router)

urlpatterns = [
    path('', index),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/token/verify/', TokenVerifyView.as_view(), name='token_verify'),
    path('api/v1/drf-auth/', include('rest_framework.urls')),
    path('api/v1/newslist/', NewsAPIListViews.as_view()),
    path('api/v1/newsedit/<int:pk>', NewsAPIUpdateView.as_view()),
    path('api/v1/newsdelete/<int:pk>', NewsAPIDestroy.as_view()),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)