from django.contrib import admin
from .models import News, Category


class NewsAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'content', 'created', 'is_published')
    list_display_links = ('id', 'title')
    list_editable = ('is_published',)
    list_filter = ('content', )


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title',)


admin.site.register(News, NewsAdmin)
admin.site.register(Category, CategoryAdmin)




