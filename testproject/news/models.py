from django.contrib.auth.models import User
from django.db import models


# Create your models here.

#title - Varchar
#created - Date
#updated - Date
#id - INT

# content - Text
# is_published - Boolean
# photo - Image

# спорт, культура, техника, политика


class News(models.Model):
    title = models.CharField(max_length=150)
    created = models.DateField(auto_now_add=True)
    updated = models.DateField(auto_now=True)
    is_published = models.BooleanField(default=True)
    content = models.TextField(blank=True)
    #photo = models.ImageField(upload_to='photos/%Y/%m/%d/', blank=True)
    category = models.ForeignKey('Category', on_delete=models.PROTECT, null=True)
    user = models.ForeignKey(User, verbose_name='Пользователь', on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'
        ordering = ['content']


class Category(models.Model):
    title = models.CharField(max_length=150)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'


