from django.forms import model_to_dict
from django.shortcuts import render
from rest_framework import generics, viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import News
from .permissions import IsAdminOrReadOnly, IsOwnerOrReadOnly
from .serializers import NewsSerializer


def index(request):
    news = News.objects.all()
    context = {'news': news, 'title': 'список новостей'}
    return render(request, 'news/index.html', context)


# class NewsViewSet(viewsets.ModelViewSet):
#     queryset = News.objects.all()
#     serializer_class = NewsSerializer


# class NewsAPIViews(generics.ListAPIView):
#     queryset = News.objects.all()
#     serializer_class = NewsSerializer

# post get
class NewsAPIListViews(generics.ListCreateAPIView):
    queryset = News.objects.all()
    serializer_class = NewsSerializer
    permission_classes = (IsAuthenticated,)


# put patch get (concrete)
class NewsAPIUpdateView(generics.RetrieveUpdateAPIView):
    queryset = News.objects.all()
    serializer_class = NewsSerializer
    permission_classes = (IsAuthenticated,)
    #authentication_classes = (TokenAuthentication, )


# delete
class NewsAPIDestroy(generics.RetrieveDestroyAPIView):
    queryset = News.objects.all()
    serializer_class = NewsSerializer
    #ßpermission_classes = (IsAdminOrReadOnly,)

    # def get(self, request):
    #     lst = News.objects.all()
    #     return Response({'get': NewsSerializer(lst, many=True).data})
    #
    #
    # def post(self, request):
    #     serializer = NewsSerializer(data=request.data)
    #     serializer.is_valid(raise_exception=True)
    #     serializer.save()
    #
    #     return Response({'post': serializer.data})
    #
    # def put(self, request, *args, **kwargs):
    #     pk = kwargs.get("pk", None)
    #     if not pk:
    #         return Response({"error": "Method PUT is not allowed"})
    #
    #     try:
    #         instance = News.objects.get(pk=pk)
    #     except:
    #         return Response({"error": "Object does not exist"})
    #
    #     serializer = NewsSerializer(data=request.data, instance=instance)
    #     serializer.is_valid(raise_exception=True)
    #     serializer.save()
    #     return Response({"post": serializer.data})
    #
    # def delete(self, request, *args, **kwargs):
    #     pk = kwargs.get("pk", None)
    #     if not pk:
    #         return Response({"error": "Method DELETE is not allowed"})
    #
    #     try:
    #         instance = News.objects.get(pk=pk)
    #     except:
    #         return Response({"error": "Object already not exist"})
    #
    #     instance.delete()
    #     return Response({"delete": "deleted object" + str(pk)})

# title = models.CharField(max_length=150)
#     created = models.DateField(auto_now_add=True)
#     updated = models.DateField(auto_now=True)
#     is_published = models.BooleanField(default=True)
#     content = models.TextField(blank=True)
#     #photo = models.ImageField(upload_to='photos/%Y/%m/%d/', blank=True)
#     category = models.ForeignKey('Category', on_delete=models.PROTECT, null=True)


