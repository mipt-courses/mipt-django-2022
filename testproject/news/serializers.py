import io
from rest_framework import serializers
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer
from news.models import News


class NewsSerializer(serializers.ModelSerializer):
   user = serializers.HiddenField(default=serializers.CurrentUserDefault())
   class Meta:
      model = News
      #fields = ['title', 'content', 'category']
      fields = '__all__'




   # def create(self, validated_data):
   #    return News.objects.create(**validated_data)
   #
   #
   # def update(self, instance, validated_data):
   #    instance.title = validated_data.get("title", instance.title)
   #    instance.content = validated_data.get("content", instance.content)
   #    instance.is_published = validated_data.get("time_update", instance.is_published)
   #    instance.category_id = validated_data.get("category_id", instance.category_id)
   #    instance.save()
   #    return instance
